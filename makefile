mips-simulator: mips-simulator.cpp            
	g++ mips-simulator.cpp -std=c++11 -o mips-simulator -O2
	
	if [ ! -d bin ]; then mkdir bin; fi
	cp ./mips-simulator ./bin/mips-simulator
                
clean:
	rm -rf *.o mips-simulator bin